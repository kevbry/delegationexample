package com.example.delegatepatternexample;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MainActivity extends Activity implements CustomerReceiver
{
    CustomerLoader loader;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        this.loader = new FakeCustomerLoader(this);
        
        //Tell the loader to start finding customers
        this.loader.findCustomersByName("Name");
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    
    ///// CustomerReceiver callback methods. MainActivity implements CustomerReceiver, and so can be passed in to the constructor
    // for FakeCustomerLoader. You should ensure that these methods are only ever called on the UI thread.
    
    @Override
    public void customerReceived(Customer customer)
    {
        //Called by CustomerLoader once a customer has been loaded
        
    }

    @Override
    public void customersReceived(ArrayList<Customer> customers)
    {
        //Many customers have been received
        
    }

    @Override
    public void failedToLoad(int errorCode, String errorMessage)
    {
        // TODO Auto-generated method stub
        
    }

}
