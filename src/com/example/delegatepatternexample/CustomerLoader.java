package com.example.delegatepatternexample;

public abstract class CustomerLoader
{
    protected CustomerReceiver receiver;
    
    public CustomerLoader(CustomerReceiver receiver)
    {
        this.receiver = receiver;
    }
    
    
    public abstract void findAllCustomers();
    public abstract void findCustomersByName(String name);
}
