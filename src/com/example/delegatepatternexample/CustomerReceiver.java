package com.example.delegatepatternexample;

import java.util.ArrayList;

/**
 * Defines an object that wants to do some work with Customer objects when they are received.
 * Methods in the class which implements this interface will be called by an instance of CustomerLoader
 * @author ins208
 *
 */
public interface CustomerReceiver
{
    public void customerReceived(Customer customer);
    public void customersReceived(ArrayList<Customer> customers);
    public void failedToLoad(int errorCode, String errorMessage);
}
