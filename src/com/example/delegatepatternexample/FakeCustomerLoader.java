package com.example.delegatepatternexample;

import java.util.ArrayList;


/**
 * A simple implementation of a CustomerLoader that provides static data
 * @author ins208
 *
 */
public class FakeCustomerLoader extends CustomerLoader
{

    public FakeCustomerLoader(CustomerReceiver receiver)
    {
        //Receiver is the object which wants to be notified when data has been loaded. It provides a disconnect between
        // the request for data to be loaded and the response containing the actual data.
        
        super(receiver);
        //Do any other setup work here
    }

    @Override
    public void findAllCustomers()
    {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        
        customers.add(new Customer("Obiwan Kenobi", "101 Main Street"));
        customers.add(new Customer("Luke Skywalker", "102 2nd avenue"));
        customers.add(new Customer("Han Solo", "245 Millenium Way"));
        
        
        //In this case, we're just directly calling customersReceived. If we were loading data from a web service or data store,
        // findAllCustomers would be starting a new thread to retrieve the data and immediately returning. The thread would be responsible
        // for calling customersReceived on receiver (on the main UI thread) once data loading had completed. Because findAllCustomers would
        // have immediately returned, the UI thread is not interrupted until the data is actually finished loading. It's a good idea to
        // have some sort of loading indicator on your UI to tell the user that something is happening, then remove it once customersReceived
        // or the error method has been called.
        this.receiver.customersReceived(customers);

    }

    @Override
    public void findCustomersByName(String name)
    {
        this.receiver.customerReceived(new Customer("Greedo", "Saskatoon Cemetary"));
    }

}
